// http module is used to make requests
const fs = require('fs');
const http = require('http');

// create server
const server = http.createServer((req, res) => {
    // minimal server configuration
    const { url, method } = req;

    console.log(url, method);

    if (url === '/') {
        res.setHeader('Content-Type', 'text/html');
        res.write('<html>');
        res.write(` <body>
                        <form action="/message" method="post">
                            <input type="text" name="message">
                            <button type="submit">Send</button>
                        </form>
                    </body>`);
        res.write('</html>');
        res.end();
    }
    if (url === '/message' && method === 'POST'){
        const body = [];
        req.on('data', (chunck) => {
            // write buffer
            console.log(chunck);
            body.push(chunck);
        }).on('end', () => {
            // concat buffer
            const parseBody = Buffer.concat(body).toString();
            console.log(parseBody);
            // save file
            fs.appendFile("Message.txt", parseBody + "\n", (err) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("Write file sucess");
                }
            })
            // send response
            res.statusCode = 302;
            res.setHeader('Location', '/');
            res.end();
        }).on('error', (err) => {
            console.log(err);
        })
    }

    // res.writeHead(200, { 'Content-Type': 'text/plain'});
    // res.end('Hello World\n');
});

// start server on port 3000
server.listen(3000);