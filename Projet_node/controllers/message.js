//Import
const { validationResult } = require('express-validator/check');
const MessageSchema = require('../models/message');

// Function for send message
exports.sendMessage = (req, res, next) => {
    // Check erreur in request
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        // Send erreur request
        return res.status(422).json({
            message: 'Validation failed, entered data is incorrect.',
            errors: errors.array()
        });
    }
    // Get title and message in the body
    const title = req.body.title;
    const message = req.body.message;

    //  Create schema for message
    const messageSchema = new MessageSchema({
        title: title,
        message: message
    });

    // Save schema in database and send response or log error
    messageSchema
        .save()
        .then(result => {
            res.status(201).json({
                message: 'Post created successfully!',
                post: result
            });
        })
        .catch(err => {
            console.log(err);
        });
}

// Function for get all messages
exports.getMessages = (req, res, next) => {
    // Get in database
    MessageSchema.find().select(['title', 'message'])
        .then(messages => {
            // Return result
            res
            .status(200)
            .json({ messages: messages });
        })
        .catch(err => {
            // Return error
            if (!err.statusCode) {
            err.statusCode = 500;
            }
            next(err);
        });
}

// Function for update a message
exports.updateMessage = (req, res, next) => {
    // Check erreur in request
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        // Send erreur request
        return res.status(422).json({
            message: 'Validation failed, entered data is incorrect.',
            errors: errors.array()
        });
    }
    // Get title and message in the body
    const title = req.body.title;
    const message = req.body.message;
    const id = req.params.id;
    MessageSchema.findById(id)
        .then(getMessage => {
            if (!getMessage) {
                const error = new Error('Could not find message.');
                error.statusCode = 404;
                throw error;
            }
            getMessage.title = title;
            getMessage.message = message;
            return getMessage.save();
        })
        .then(result => {
            res.status(200).json({ message: 'Message updated!', post: result });
        })
        .catch(err => {
            if (!err.statusCode) {
              err.statusCode = 500;
            }
            next(err);
          });
}

// Function for delete a message
exports.deleteMessage = (req, res, next) => {
    const id = req.params.id;
    MessageSchema.findById(id)
        .then(getMessage => {
            if (!getMessage) {
                const error = new Error('Could not find message.');
                error.statusCode = 404;
                throw error;
            }
            return MessageSchema.findByIdAndRemove(id);
        })
        .then(result => {
            console.log(result);
            res.status(200).json({ message: 'Deleted message.' });
        })
        .catch(err => {
            if (!err.statusCode) {
              err.statusCode = 500;
            }
            next(err);
        });
}