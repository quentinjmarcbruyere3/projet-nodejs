// Import
const express = require('express');
const messageController = require('../controllers/message');
const isAuth = require('../middleware/is-auth');

// Create router 
const router = express.Router();

// Router redirect
// POST /topic/message
router.post('/message', isAuth, messageController.sendMessage);

// GET /topic/messages
router.get('/messages', isAuth, messageController.getMessages);

// PUT /topic/message/:id
router.put('/message/:id', isAuth, messageController.updateMessage);

// DELETE /topic/message/:id
router.delete('/message/:id', isAuth, messageController.deleteMessage);

// Export modules
module.exports = router;