// Import
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const messageRoute = require('./routes/message');
const authRoute = require('./routes/auth');
const multer = require('multer');
const path = require('path');
const fileRoute = require('./routes/file');
require('dotenv').config()

// Create app
const app = express();

// Import swagger
const swaggerUi = require('swagger-ui-express');
swaggerDocument = require('./swagger.json');

// Get the file storage
const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'images');
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  }
});

// Filter file type
const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === 'image/png' ||
    file.mimetype === 'image/jpg' ||
    file.mimetype === 'image/jpeg'
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

// Parse body
app.use(bodyParser.json());

// Use multer in the app
app.use(
  multer({ storage: fileStorage, fileFilter: fileFilter }).single('image')
);
//app.use('/images', express.static(path.join(__dirname, 'images')));

// Response header
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});

// Router redirect
app.use('/topic', messageRoute);
app.use('/auth', authRoute);
app.use('/file', fileRoute);

// Use swagger
app.use('/api-docs', swaggerUi.serve,  swaggerUi.setup(swaggerDocument));

// Use error log in the app
app.use((error, req, res, next) => {
  console.log(error);
  const status = error.statusCode || 500;
  const message = error.message;
  res.status(status).json({ message: message });
});

// Start database connexion and start app
mongoose
  .connect(process.env.DB_CONNECTION)
  .then(result => {
    app.listen(8080);
  })
  .catch(err => console.log(err));