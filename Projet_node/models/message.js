// Import
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Declare schema for a message
const messageSchema = new Schema(
  {
    title: {
      type: String,
      required: true
    },
    message: {
      type: String,
      required: true
    }
  },
  { timestamps: true }
);

// Export the schema
module.exports = mongoose.model('messageSchema', messageSchema);