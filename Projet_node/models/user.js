// Import
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Describe schema for the user 
const userSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  pseudo: {
    type: String,
    required: true
  },
  dateNaissance: {
    type: String,
    required: true
  },
  presentation: {
    type: String,
    required: false
  },
  photo: {
    type: String,
    required: false
  },
  pays: {
    type: String,
    required: false
  }
});

// Export the schema
module.exports = mongoose.model('User', userSchema);
