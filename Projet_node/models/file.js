// Import
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Describe schema for a file
const fileSchema = new Schema(
  {
    title: {
      type: String,
      required: true
    },
    imageUrl: {
      type: String,
      required: true
    },
    content: {
      type: String,
      required: true
    },
    creator: {
      type: String,
      required: true
    }
  },
  { timestamps: true }
);

// Export the schema 
module.exports = mongoose.model('File', fileSchema);