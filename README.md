# Prise de note

## Important

Pas d'utilisation de var

## Qu'est-ce que Node.js

Un environnement d'exécution open-source, multi-plateform qui permet de lancer des applications js côté serveur.

## Modules
**http** => permet de lancer un serveur web

## Objet request
Header: métadonnée d'un requête http

## Modele MVC 
Avec Express route et controleur sont très liés  
Réaliser une couche service pour faire le pont entre la base de donnée et le modèle

## Réalisation
Api backend  
Retourne uniquement du JSON

## Method
GET  
POST  
PUT  
PATCH  
DELETE

## Consigne
- Inscription/connexion
- Quelques endpoints
- Connexion avec une BDD
- Swagger (bonus)
- Commit régulièrement (branche)
- Upload de fichier
- Validation pour les POST
- Stockage propre (hashage mdp)
- Syntaxe moderne

# Commande pour l'application

## Installer les dépendances
```
npm install
```

## Lancer l'application
```
node index.js
```

## Accéder à la documentation swagger
http://localhost:8080/api-docs/

## Postman

Vous trouverez dans le répertoire principal du projet Projet_node une exportation de la collection Postman avec tous les endpoints.

*Projet réalisé par Quentin BRUYERE*